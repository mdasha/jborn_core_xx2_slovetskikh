package s08;

public class File {

    String path;

    public File(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

}