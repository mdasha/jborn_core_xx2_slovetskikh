package s08;

import java.io.*;
import java.math.BigDecimal;

public class Task01 {
    public static void main(String[] args) {

        //Файл, из которого считываем данные
        File inFile = new File("C:\\Users\\Дарья\\Documents\\JBorn_Core_XX2_Slovetskikh\\src\\s08\\1.txt");    // создаем новый объект типа File, у которого есть определенный путь

        // Файл, в который записываем рассчитанную сумму
        File outFile = new File("C:\\Users\\Дарья\\Documents\\JBorn_Core_XX2_Slovetskikh\\src\\s08\\2.txt");

        //Записывем посчитанную сумму в файл outFile
        writeToFile(inFile, outFile);
    }

    // Метод, считывающий данные из файла, вычисляющий сумму чисел из первой строки файла и записывающий ее во второй файл
    private static void writeToFile(File inFile, File outFile) {
        BigDecimal sum = new BigDecimal("0");
        try {
            String firstLine = readFirstLine(inFile);
            if (firstLine != null) {
                System.out.println("Первая строка из файла " + "\n" + inFile.getPath() + "\n\n" + firstLine + "\n");
                String[] numbers = firstLine.split(" ");
                for (String number : numbers) {
                    try {
                        BigDecimal bigDecimal = new BigDecimal(number);
                        sum = sum.add(bigDecimal);
                        // Если сума превышает Long.MAX_VALUE, то обнуляем сумму и выходим из цикла, дальше не суммируем
                        if (sum.compareTo(new BigDecimal(Long.MAX_VALUE)) > 0) {
                            System.out.println("Сумма превышает Long.MAX_VALUE");
                            sum = new BigDecimal("0");
                            break;
                        }
                    }
                    // Ловим исключение: если один из введенных через пробел символов - не число или превышено максимальное значение Long
                    catch (NumberFormatException e) {
                        sum = new BigDecimal("0");  // обнуляем сумму, т.к. одно из введенных значений неверное (символ или превышено максимальное значение LONG)
                        System.out.println("Введенное значение не может быть преобразовано в LONG (число больше Long.MAX_VALUE или символ): " + e);
                        break;
                    }
                }
                System.out.println("Сумма чисел в первой строке равна: " + sum + "\n");
            } else {
                System.out.println("В файле нет первой строки!");
            }
            // Ловим исключение. Если нет файла по адресу path или доступ к файлу запрещен
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            // Записывем полученные данные в файл (если полученная сумма не ноль)
            if (sum.compareTo(new BigDecimal("0")) == 1) {
                writeToFirst(outFile, sum.toString());
                System.out.println("Посчитанная сумма записана в файл " + outFile.getPath());
                // Если полученная сумма 0, то данные не записываем в файл, а выводим сообщение в консоль
            } else {
                System.out.println("Не удалось посчитать сумму чисел в первой строке файла. Данные в файл " + outFile.getPath() + " не записаны.");
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    // Считывание первой строки из файла
    private static String readFirstLine(File file) throws IOException {
        FileInputStream stream = new FileInputStream(file.getPath());
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String first = reader.readLine();
        reader.close();
        stream.close();
        return first;
    }

    // Запись в файл
    private static void writeToFirst(File file, String value) throws IOException {
        FileWriter writer = new FileWriter(file.getPath(), false);
        writer.write(value);
        writer.flush();
        writer.close();
    }
}
