package s05;

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        String latinChar = requestString().toUpperCase(); // т.к. в ENUM все буквы указаны в верхнем регистре, то переводим все вводимые значения в верхний регистр

        // Обрезаем введенную строку до первого символа (если она больше 1 символа)
        if (latinChar.length() > 1) {
            latinChar = latinChar.substring(0, 1);
            System.out.println("Введено более одной буквы, определяем порядковый номер первой буквы - " + latinChar);
        }

        // Проверка, содержится ли вводимая буква в латинском алфавите. Если нет, то выдается сообщение об ошибке
        try {
            LatinAlphabet latinAlphabet = LatinAlphabet.valueOf(latinChar);
            System.out.println("Номер буквы в алфавите " + latinAlphabet.getPos());
        } catch (IllegalArgumentException e) {
            System.out.println("Вы ввели не ту букву. Может быть, это и вовсе не буква. Попробуйте еще раз");
        }
    }

    // Вводим с клавиатуры букву латинского алфавита
    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите букву латинского алфавита:");
        return scanner.nextLine();
    }

}
