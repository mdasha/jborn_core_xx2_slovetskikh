package s05;

import java.util.*;

public class SuperHero extends Hero {

    private final int godMinutes;
    private final String time;
    Date date = new Date();

    public SuperHero(int power, int godMinutes) {
        super(power);
        this.godMinutes = godMinutes;
        this.time = date.toString();
    }


    @Override
    public String toString() {
        return super.toString()
                + "Время неуязвимости  \t" + repeatAsterisk(godMinutes / 10) + "\n"
                + "Время создания супергероя  \t" + time + "\n";
    }

    @Override
    public void getHitted(Hero hero) {
        Date date2 = new Date();
        System.out.println("Время удара: " + date2);

        if ((date2.getTime() - date.getTime()) > godMinutes) {
            this.health = getHealth() - hero.power;  // здоровье не теряем, если времени прошло меньше, чем время неуязвимости
        }
        System.out.println("Время от создания супергероя до удара (мс): " + (date2.getTime() - date.getTime()));
    }
}
