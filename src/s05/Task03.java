package s05;

import java.util.Scanner;

public class Task03 {

    public static void main(String[] args) {
        System.out.println("Введите первое число");
        System.out.println("Введите действительную часть комплексного числа: ");
        double re1 = requestNumber();
        System.out.println("Введите мнимую часть комплексного числа: ");
        double im1 = requestNumber();

        System.out.println("Введите второе число");
        System.out.println("Введите действительную часть комплексного числа: ");
        double re2 = requestNumber();
        System.out.println("Введите мнимую часть комплексного числа: ");
        double im2 = requestNumber();

        // Создаем экземпляры класса ComplexNumber - два комплексных числа
        ComplexNumber complexNumber1 = new ComplexNumber(re1, im1);
        ComplexNumber complexNumber2 = new ComplexNumber(re2, im2);

        // Сравниваем содержимое объектов комплексных чисел
        if (complexNumber1.equals(complexNumber2)) {
            System.out.println("Комплексные числа равны по содержимому");
        } else {
            System.out.println("Комплексные числа не равны по содержимому");
        }

        // Распечатаем значения hashCode для обоих чисел:
        System.out.println("HashCode первого комплексного числа: " + complexNumber1.hashCode());
        System.out.println("HashCode второго комплексного числа: " + complexNumber2.hashCode());
    }

    private static double requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }
}
