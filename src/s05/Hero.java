package s05;

import java.util.Collections;

public class Hero {
    static int INITIAL_HEALTH = 100;

    protected int health;
    protected final int power;
    protected final int speed;
    protected int health1;

    public Hero() {
        health = INITIAL_HEALTH;
        power = 50;
        speed = 50;
    }

    public Hero(int power) {
        this.health = INITIAL_HEALTH;
        this.power = power % 100;
        this.speed = 100 - this.power;
    }

    public int getHealth() {
        return this.health;
    }

    // Метод нанесения урона другому герою
    public void hit(Hero hero) {
        hero.getHitted(this);
    }

    // Метод получения поражения от другого героя
    public void getHitted(Hero hero) {
        this.health = this.health - hero.power;
    }

    @Override
    public String toString() {
        return "health \t" + repeatAsterisk(health / 10) + "\n" +
                "power \t" + repeatAsterisk(power / 10) + "\n" +
                "speed \t" + repeatAsterisk(speed / 10) + "\n";
    }

    protected static String repeatAsterisk(int times) {
        return String.join("", Collections.nCopies(times, "*"));
    }


}
