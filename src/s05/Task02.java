package s05;

public class Task02 {
    public static void main(String[] args) {


        Addition addition = new Addition();
        System.out.println("1. Сумма двух операндов " + addition.calculate(addition.leftOperand, addition.rightOperand));
        System.out.println("Результат предыдущего вычисления: " + addition.returnPrevious());
        Addition addition2 = new Addition();
        System.out.println("2. Сумма двух операндов " + addition2.calculate(addition2.leftOperand, addition2.rightOperand));
        System.out.println("Результат предыдущего вычисления (1): " + addition2.returnPrevious());
        Multiplication multiplication = new Multiplication();
        System.out.println("3. Произведение двух операндов " + multiplication.calculate(multiplication.leftOperand, multiplication.rightOperand));
        System.out.println("Результат предыдущего вычисления (2): " + multiplication.returnPrevious());
        Multiplication multiplication2 = new Multiplication();
        System.out.println("4. Произведение двух операндов " + multiplication2.calculate(multiplication2.leftOperand, multiplication2.rightOperand));
        System.out.println("Результат предыдущего вычисления (3): " + multiplication2.returnPrevious());
        Addition addition3= new Addition();
        System.out.println("5. Сумма двух операндов " + addition3.calculate(addition3.leftOperand, addition3.rightOperand));
        System.out.println("Результат предыдущего вычисления (4): " + addition3.returnPrevious());
        Multiplication multiplication3 = new Multiplication();
        System.out.println("6. Произведение двух операндов " + multiplication3.calculate(multiplication3.leftOperand, multiplication3.rightOperand));
        System.out.println("Результат предыдущего вычисления (5): " + multiplication3.returnPrevious());
    }



}
