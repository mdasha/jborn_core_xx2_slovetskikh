package s05;

import java.util.Scanner;

public class Operation {

    int leftOperand; // левый операнд
    int rightOperand; // правый операнд
    static int calculate1; // результат предыдущего вычисления
    static int calculate2; // текущее вычисление


    Operation() {
        System.out.println(" ");
        System.out.println("Введите первый операнд: ");
        leftOperand = requestNumber(); // вводим левый операнд с клавиатуры
        System.out.println("Введите второй операнд: ");
        rightOperand = requestNumber(); // вводим правый операнд с клавиатуры
    }

    public int calculate(int leftOperand, int rightOperand) {
        calculate2 = calculate1;
        calculate1 = leftOperand + rightOperand;
        return calculate1;
    }


    int returnPrevious() {
        return calculate2;
        }

    private int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
