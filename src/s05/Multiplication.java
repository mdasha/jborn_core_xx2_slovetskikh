package s05;

public class Multiplication extends Operation {


    @Override
    public int calculate(int leftOperand, int rightOperand) {
        calculate2 = calculate1;
        calculate1 = leftOperand * rightOperand;
        return calculate1;
    }


    @Override
    int returnPrevious() {
        return calculate2;
    }
}
