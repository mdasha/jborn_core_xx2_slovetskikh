package s05;

import java.util.Scanner;

public enum LatinAlphabet {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z;

    // Метод определения порядкового номера буквы
    public int getPos() {
       return (ordinal() + 1);
    }
}



