package s13;

public class User {
    @Column("first_name")
    private String firstName;

    @Column("last_name")
    private String lastName;

    @Column("age")
    private Integer age;

    @Column("city")
    private String city;




    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    public Integer getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "User {" +
                "firstName = " + firstName + '\'' +
                ", lastName = " + lastName + '\'' +
                ", age = " + age + '\'' +
                ", city = " + city + '\'' +
                '}';
    }

}
