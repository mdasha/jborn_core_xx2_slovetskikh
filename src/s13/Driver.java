package s13;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Driver {

    @NotEmpty
    ArrayList<String> name;

    private LocalDate dateOfBirthLocalDate;

    @NotNull()
    String drivingLicenseSeries;

    String drivingLicenseNumber;

    LocalDate licenseNumberDateLocalDate;

    @Max(age = 45)
    int age;

    @Min(height = 145)
    int height;

    String annualPhysicalExam;

    boolean actualLicence;

    @RegExp(regexp = "[ABD]")
    String licenceCategory;

    public Driver(ArrayList<String> name, int height, String dateOfBirth, String drivingLicenseSeries, String drivingLicenseNumber, String licenseNumberDate, String annualPhysicalExam, String licenceCategory) {
        this.name = name;
        this.height = height;
        this.dateOfBirthLocalDate = LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("dd-LL-yyyy"));
        this.drivingLicenseSeries = drivingLicenseSeries;
        this.drivingLicenseNumber = drivingLicenseNumber;
        this.licenseNumberDateLocalDate = LocalDate.parse(licenseNumberDate, DateTimeFormatter.ofPattern("dd-LL-yyyy"));
        this.age = (LocalDate.now()).getYear() - (dateOfBirthLocalDate.getYear());  // вычисляем возраст водителя
        this.annualPhysicalExam = annualPhysicalExam; // ежегодная диспансеризация (да или нет)
        this.actualLicence = (LocalDate.now().isAfter(licenseNumberDateLocalDate));
        this.licenceCategory = licenceCategory;
    }

    public Driver(ArrayList<String> name, int height, String dateOfBirth,  String annualPhysicalExam, String licenceCategory) {
        this.name = name;
        this.height = height;
        this.dateOfBirthLocalDate = LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("dd-LL-yyyy"));
        this.age = (LocalDate.now()).getYear() - (dateOfBirthLocalDate.getYear());  // вычисляем возраст водителя
        this.annualPhysicalExam = annualPhysicalExam; // ежегодная диспансеризация (да или нет)
        this.licenceCategory = licenceCategory;
    }

    @Override
    public String toString() {
        String a = "";
        if (drivingLicenseSeries != null && drivingLicenseNumber !=null  && licenseNumberDateLocalDate != null) {
            a  = "Номер и серия прав: " + drivingLicenseSeries + " " + drivingLicenseNumber + "\n" +
                    "Права выданы: " + licenseNumberDateLocalDate + "\n";
        }

        return
                "Проверяем водителя: " + name.get(0) + " " + name.get(1) + " " + name.get(2) + "\n" +
                        "Дата рождения: " + dateOfBirthLocalDate + "\n" +
                        "Возраст водителя: " + age + "\n" +
                        "Рост водителя: " + height + "\n" +
                         a +
                        "Ежегодный медицинский осмотр пройден: " + annualPhysicalExam + "\n" +
                        "Категория прав: " + licenceCategory;
    }

    public List getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getHeight() {return height;}

    public LocalDate getDateOfBirth() {
        return dateOfBirthLocalDate;
    }

    public String getAnnualPhysicalExam() {
        return annualPhysicalExam;
    }

    public boolean getActualLicense() {
        return actualLicence;
    }

    public String getLicenceCategory() {
        return licenceCategory;
    }

}