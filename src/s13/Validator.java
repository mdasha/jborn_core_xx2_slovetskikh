package s13;

import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Validator {


    boolean validate(Object obj) throws IllegalAccessException {

        System.out.println(obj);  // Распечатываем объект (нашего водителя)
        System.out.println();
        System.out.println("Проверки: ");

        // Создаем переменную, в которую будем записывать результат и массив с ошибками валидации
        int result = 0;
        int[] mistakes = {0, 0, 0, 0, 0};

        Class aClass = obj.getClass();

        for (Field field : aClass.getDeclaredFields()) {
            Max max = field.getAnnotation(Max.class);
            Min min = field.getAnnotation(Min.class);
            RegExp regexp = field.getAnnotation(RegExp.class);
            NotNull notNull = field.getAnnotation(NotNull.class);
            NotEmpty notEmpty = field.getAnnotation(NotEmpty.class);


            // Проверяем возраст
            if (max != null) {
                boolean age = Integer.valueOf(field.get(obj).toString()) < max.age();
                System.out.println("Возраст меньше " + max.age() + ": " + age);
                if (age) {
                    mistakes[1] = 1;
                }
            }

            // Проверяем рост
            if (min != null) {
                boolean height = Integer.valueOf(field.get(obj).toString()) > min.height();
                System.out.println("Рост больше " + min.height() + ": " + height);
                if (height) {
                    mistakes[0] = 1;
                }
            }

            // Проверяем категорию прав
            if (regexp != null) {
                // Проверка регулярного выражения.
                Pattern pattern = Pattern.compile(regexp.regexp());
                Matcher matcher = pattern.matcher(field.get(obj).toString());
                boolean regExp = matcher.find();
                System.out.println("Есть права категории A, B или D: " + regExp);
                if (regExp) {
                    mistakes[4] = 1;
                }
            }

            // Проверяем наличие водительской лицензии
            if (notNull != null) {
                boolean driverLicense = field.get(obj) != null;
                System.out.println("Есть ли водительская лицензия: " + driverLicense);
                if (driverLicense) {
                    mistakes[2] = 1;
                }
            }

            // Проверяем, заполнено ли ФИО
            if (notEmpty != null) {
                String nameField = field.get(obj).toString();
                nameField = nameField.replace("[", "")
                        .replace("]", "")
                        .replace(",", "");
                boolean name = !nameField.equals("  ");
                System.out.println("ФИО заполнено: " + name);
                if (name) {
                    mistakes[3] = 1;
                }
            }
        }

        // Итерируемся по массиву ошибок и считаем результат. Если все 5 условий выполнены, то результат равен 5.
        for (int mistake : mistakes) {
            result += mistake;
        }

        // Выводим сообщения о пройденных проверках и найденных ошибках
        System.out.println();
        System.out.println("Всего проверок: 5");
        System.out.println("Пройдено проверок: " + result);

        if (result != 5) {
            System.out.println();
            System.out.println("Не пройденные проверки: ");
        }

        if (mistakes[0] == 0) {
            System.out.println("У вас слишком маленький рост - вы ниже 145 см");
        }
        if (mistakes[1] == 0) {
            System.out.println("Вы не проходите по возрасту. Вы старше 45 лет");
        }
        if (mistakes[2] == 0) {
            System.out.println("У вас нет водительской лицензии");
        }
        if (mistakes[3] == 0) {
            System.out.println("Вы не заполнили ФИО");
        }
        if (mistakes[4] == 0) {
            System.out.println("У вас нет прав категории A, B или D");
        }
        // Если все провреки пройдены, то возвращаем true
        return result == 5;
    }
}
