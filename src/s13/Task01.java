package s13;

import java.util.ArrayList;

public class Task01 {
    public static void main(String[] args) throws IllegalAccessException, ClassNotFoundException {

        // Создаем объект нового водителя
        System.out.println("Водитель 1");
        ArrayList<String> name = new ArrayList<>();
        name.add("Дарья");
        name.add("Алексеевна");
        name.add("Словецких");
        Driver driver = new Driver(name, 158, "31-08-1979", "23 KC", "018834", "10-10-2018", "да", "D");

        Validator validator = new Validator();
        System.out.println();
        boolean condition = validator.validate(driver);
        isValid(condition); // Печать комментариев при проверке на валидность

        System.out.println("==================================================================");
        System.out.println("Водитель 2");

        ArrayList<String> name2 = new ArrayList<>();
        name2.add("Алексей");
        name2.add("Николаевич");
        name2.add("Словецких");
        Driver driver2 = new Driver(name2, 140, "31-08-2008",  "да", "-");

        Validator validator2 = new Validator();
        System.out.println();
        boolean condition2 = validator2.validate(driver2);
        isValid(condition2); // Печать комментариев при проверке на валидность
    }


    private static void isValid(boolean condition) {
        if (condition) {
            System.out.println();
            System.out.println("Заключение");
            System.out.println("Поздравляем, все проверки пройдены. Вы можете водить автомобиль!");
        } else {
            System.out.println();
            System.out.println("Заключение");
            System.out.println("Проверки не пройдены. Вы не можете водить автомобиль!");
        }
    }
}
