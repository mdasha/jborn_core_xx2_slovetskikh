package s09;

import java.io.*;

public class Task04 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fos = new FileOutputStream("src/s09/task4_externalizable.txt");
        FileInputStream fis = new FileInputStream("src/s09/task4_externalizable.txt");

        // Сериализация
        try (
                ObjectOutputStream oos = new ObjectOutputStream(fos)
        ) {
            User user = new User("Дарья Алексеевна", "Словецких");
            oos.writeObject(user);
        } catch (IOException e) {
            System.out.println(e);
        }

        //Десериализация
        try (
                ObjectInputStream ois = new ObjectInputStream(fis)
        ) {
            User user2 = (User) ois.readObject();
            System.out.println(user2);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
