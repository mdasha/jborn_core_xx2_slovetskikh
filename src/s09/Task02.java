package s09;

import java.io.*;

public class Task02 {
    public static void main(String[] args) throws IOException {
        File outFile = new File("src/s09/text01.txt");  // Файл для записи сгенерированных чисел
        File positive_numbers = new File("src/s09/positive_numbers.txt"); // Файл для записи положительных чисел
        File negative_numbers = new File("src/s09/negative_numbers.txt"); // Файл для записи отрицательных чисел


        // Генерим 100 положительных и 100 отрицательных чисел и записываем сгенерированные целые числа в строку
        String stringOfNumbers = createStringOfNumbers(100, 1) + createStringOfNumbers(100, -1);

        // Записываем строку stringOfNumbers в файл outFile
        try (FileWriter writer = new FileWriter(outFile, false))  {
            writer.write(stringOfNumbers);
        } catch (IOException e) {
            System.out.println(e);
        }

        try (
                FileInputStream stream = new FileInputStream(outFile);
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream))
                ) {
            // Считываем записанные данные из файла
            String outStringOfNumbers = reader.readLine();
            System.out.println("Прочитанные данные из файла: " + outStringOfNumbers);

            String[] positiveNegativeArray = outStringOfNumbers.split(" ");

            // Раскладываем все данные в две строки positiveNumbers и negativeNumbers в зависимсоти от знака числа
            StringBuilder positiveNumbers = new StringBuilder();
            StringBuilder negativeNumbers = new StringBuilder();
            for (int i = 0; i < positiveNegativeArray.length; i++) {
                if (Integer.parseInt(positiveNegativeArray[i]) > 0) {
                    positiveNumbers.append(positiveNegativeArray[i]).append(" ");
                } else {
                    negativeNumbers.append(positiveNegativeArray[i]).append(" ");
                }
            }

            // Записываем строку с положительными целыми числами в файл positive_numbers,
            // Записываем строку с отрицательными целыми числами в файл negative_numbers
            try (
                    FileWriter writer2 = new FileWriter(positive_numbers, false);
                    FileWriter writer3 = new FileWriter(negative_numbers, false);
                    ){
                writer2.write(positiveNumbers.toString());
                writer3.write(negativeNumbers.toString());
            } catch (IOException e) {
                System.out.println(e);
            }
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    // Генерация ряда случайных целых чисел
    private static String createStringOfNumbers(int quantity, int sign) {
        StringBuilder stringOfNumbers = new StringBuilder();
        for (int i = 0; i < quantity; i++) {
            int number = (int) (Math.random() * 1000 + 1) * sign;  // задаем рандомное значение числа
            stringOfNumbers.append(number).append(" ");
        }
        return stringOfNumbers.toString();
    }
}
