package s09;

import java.io.IOException;
import java.nio.file.*;

public class Task01 {
    public static void main(String[] args) {
        Path baseDir = Paths.get("C:/Users/Дарья/Documents/JBorn_Core_XX2_Slovetskikh");
        StringBuilder result = findFile(baseDir, "Task01.java");
        System.out.println(result);
    }

    public static StringBuilder findFile(Path path, String fileName) {
        StringBuilder result = new StringBuilder();
        try {
            DirectoryStream<Path> dirStream = Files.newDirectoryStream(path);
            for (Path pathes : dirStream) {
                if (Files.isDirectory(pathes)) {
                    result.append(
                            findFile(pathes, fileName)
                    );
                } else {
                    if (pathes.getFileName().toString().contains(fileName)) {
                        result.append(pathes).append("\n");
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }
}