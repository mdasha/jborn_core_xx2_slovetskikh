package s09;

import java.io.*;
import java.nio.charset.Charset;

public class Task03
{
    public static void main(String[] args) throws IOException {
        InputStream fis = new FileInputStream("src/s09/text_input_stream.txt");
        System.out.println(readAsString(fis, Charset.forName("UTF-8")));
    }

    private static String readAsString(InputStream inputStream, Charset charset) {
        StringBuilder stringBuilder = new StringBuilder();
        try (
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, charset))
                ) {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }

        } catch (IOException e) {
            System.out.println(e);
        }
        return stringBuilder.toString();
    }
}
