package s02;

import java.util.Scanner;

import static java.lang.Math.*;

public class Task6 {
    public static void main(String[] args) {
        int r = requestNumber();

        double l = 2 * PI * r;
        double s = PI * r * r;

        System.out.println("Длина окружности: " + l);
        System.out.println("Площадь круга: " + s);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите радиус (целое число):");
        return scanner.nextInt();
    }
}
