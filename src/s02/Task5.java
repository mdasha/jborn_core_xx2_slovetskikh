package s02;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        System.out.println("Введите три числа");

        int x = requestNumber();
        int y = requestNumber();
        int z = requestNumber();

        if ((x * x + y * y) == z * z)
            System.out.println("Введенные числа - тройка Пифагора. Гипотенуза - третье число");
        else if ((y * y + z * z) == x * x)
            System.out.println("Введенные числа - тройка Пифагора. Гипотенуза - первое число");
        else if ((x * x + z * z) == y * y)
            System.out.println("Введенные числа - тройка Пифагора. Гипотенуза - второе число");
        else
            System.out.println("Введнные числа - не тройка Пифагора");
    }


    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
