package s02;

import java.util.Scanner;
import static java.lang.Math.*;

public class Task4 {
    public static void main(String[] args) {
        System.out.println("Введите координаты первой точки x1 и y1");
        double x1 = requestNumber();
        double y1 = requestNumber();
        System.out.println("Введите координаты второй точки x2 и y2");
        double x2 = requestNumber();
        double y2 = requestNumber();

        double d = sqrt(pow((x1 - x2),2) + pow((y1 - y2), 2));

        System.out.println("Расстояние между двумя точками равно: " + d);

    }

    static double requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextDouble();
    }
}
