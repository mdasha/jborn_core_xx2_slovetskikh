package s02;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {

        int randomNumber = requestNumber();
        int multi;
        System.out.println("Таблица умножения числа " + randomNumber + " на числа от 1 до 10");

        for (int i = 1; i <=10; i++) {
            multi = randomNumber * i;
            System.out.println(randomNumber + "*" + i + "=" + multi);
        }
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
