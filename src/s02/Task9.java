package s02;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        int randomNumber = requestNumber();
        int sum = 0; // сумма цифр
        int remainder = randomNumber;

        while (randomNumber  > 0) {
            remainder = randomNumber  % 10;  // остаток от деления числа N на десять
            randomNumber  = randomNumber  / 10; // уменьшаем исходное число в 10 раз, на следующей итерации ищем остаток уже от него
            sum = sum + remainder; // ищем сумму цифр исходного числа, каждый раз прибавляя к полученной на прошлом шаге сумме новый остаток от деления на 10
        }


        System.out.println("Сумма цифр введенного числа " + randomNumber + "равна: " + sum);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
