package s02;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int n = requestNumber();


        // Проверяем, является ли введенное число двузначным. Если нет, то просим ввести двузначное число заново.
        if (n / 10 > 9 | n / 10 < 1) {
            System.out.println("Введите двузначное число");
            n = requestNumber();
        }

        int m = n / 10; // Число десятков во введенном числе
        int l = n % 10; // Число единиц во введенном числе
        int k = m + l; // Сумма цифр введенного двузначного числа
        int d = m * l; // Произведение цифр введенного двузначного числа


        System.out.println("Число десятков во введенном двузначном числе: " + m);
        System.out.println("Число единиц во введенном двузначном числе: " + l);
        System.out.println("Сумма цифр введенного двузначного числа: " + k);
        System.out.println("Произведение цифр введенного двузначного числа: " + d);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
