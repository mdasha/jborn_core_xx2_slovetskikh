package s02;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {

        System.out.println("Введите размер массива, больше 0: ");
        int l = requestNumber(); // задаем размер массива
        int array[];
        array = new int[l];
        int sum = 0; // Вводим переменную для расчета суммы четных элементов массива
        int multy = 1; // Вводим перменную для расчета произведения всех нечетных элементов массива

        System.out.println("Введите элементы массива длины " + l);

        for (int i = 0; i < l; i++) {
            array[i] = requestNumber();
            if ( i % 2 == 0)
                multy = multy * array[i];
            else
                sum = sum + array[i];
        }

        System.out.println("Сумма всех четных элементов массива равна: " + sum);
        System.out.println("Произведение всех нечетных элементов массива равно: " + multy);

    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
