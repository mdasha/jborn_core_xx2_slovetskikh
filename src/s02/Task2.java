package s02;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
       int n = requestNumber();

       if (n > 86400) {
           System.out.println("В сутках 86400 с, введите число меньше 86400");
           n = requestNumber();
       }

       int m = n / 3600; // Прошло полных часов с начала суток

       int l = (n % 3600) / 60;  // Прошло полных минут с начала очередного часа

       int k = (n % 60);  // Прошло полных секунд с начала очередной минуты


        // Выведем полученные данные на экран
        System.out.println("Прошло полных часов с начала суток: " +  m + " час");
        System.out.println("Прошло полных часов с начала очередного часа: " + l + " мин");
        System.out.println("Прошло полных секунд с начала очередной минуты: " + k + " с");
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }
}
