package s06;

import java.util.Scanner;

public class Task04 {

    public static void main(String[] args) {

        System.out.println("Введите фразу (только буквы и пробелы) ");
        String message = requestString();

        System.out.println("Введите минимальное количество слов в вводимой фразе: ");
        int m = requestNumber();
        System.out.println("Введите максимальное количество букв в каждом из вводимых слов фразы: ");
        int n = requestNumber();

        CheckString checkString = new LowerCaseHandler(message);
        checkString.linkWith(new AlphabetHandler()).linkWith(new LongWordHandler(m, n));
        CheckMessage checkMessage = new CheckMessage();
        checkMessage.setCheckString(checkString);

        if (checkMessage.logIn(message)) {
            System.out.println("Все проверки пройдены: " + checkString.handleMessage(message));
        } else {
            System.out.println("Проверки не пройдены: " + null);
        }
    }

    private static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
