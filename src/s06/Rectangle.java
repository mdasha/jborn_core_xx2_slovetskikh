package s06;

public class Rectangle extends Shape {

    protected double a; // короткая сторона прямоугольника
    private double b; // длинная сторона прямоугольника


    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculatePerimeter() {
        return  perimeter = 2*a + 2*b;
    }


    @Override
    public double calculateArea() {
        return square = a*b;
    }
}
