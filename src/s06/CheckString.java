package s06;

public class CheckString implements Handler {

    private CheckString next;
    protected String message;

    public CheckString linkWith(CheckString next) {
        this.next = next;
        return next;
    }

    protected String checkNext(String message) {
        if (next != null) {
            return next.handleMessage(message);
        }
           return message;
    }

    @Override
    public String handleMessage(String message) {
        return checkNext(message.toLowerCase().trim());
    }
}
