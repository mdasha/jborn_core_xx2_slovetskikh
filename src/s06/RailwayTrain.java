package s06;

class RailwayTrain {

    private int sumMass; // суммарная грузоподъемность всех вагонов
    private Carriadge[] carriadges; // массив вагонов


    RailwayTrain(Carriadge[] carriadges) {
        this.carriadges = carriadges;
        sumMass = 0;
    }


    int sumMass(int quantity) {
        for (int i = 0; i < quantity; i++) {
            sumMass += carriadges[i].getMass();
        }
        return sumMass;
    }

    double averageMass(int quantity) {
        return (double) sumMass / quantity;
    }
}
