package s06;

public class AlphabetHandler extends CheckString {

    @Override
    public String handleMessage(String message) {

        // делим строку на символы

        if (message == null || !message.matches("^[A-Za-zAА-Яа-я\\s]+$")) {
            System.out.println("Ошибка ввода: фраза состоит не только из букв и пробелов или фразы нет");
            return null;
        }
        return checkNext(message);
    }
}
