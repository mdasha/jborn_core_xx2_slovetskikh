package s06;
public abstract class Shape implements Summarize {
    protected double perimeter;  // периметр фигуры
    protected double square;  // площадь фигуры

    public abstract double calculatePerimeter();
    public abstract double calculateArea();
}
