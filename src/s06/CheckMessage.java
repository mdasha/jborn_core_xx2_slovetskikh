package s06;

public class CheckMessage {
    private CheckString checkMessage;


    public void setCheckString(CheckString checkString) {
        this.checkMessage = checkString;
    }

    public boolean logIn(String message) {
        return checkMessage.handleMessage(message) != null;
    }


}
