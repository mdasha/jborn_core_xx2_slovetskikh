package s06;

public class Triangle extends Shape {

    private double a; // первая сторона треугольника
    private double b; // вторая сторона треугольника
    private double c; // угол между сторонами в градусах


    protected Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double calculatePerimeter() {
        return perimeter = a + b + c;
    }

    @Override
    public double calculateArea() {
        double halfperimeter = perimeter/2;
        square = Math.sqrt(halfperimeter * (halfperimeter - a) * (halfperimeter - b) * (halfperimeter - c));
        return square;
    }
}