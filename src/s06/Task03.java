package s06;

import java.util.Scanner;

public class Task03 {

    public static void main(String[] args) {


        System.out.println("Введите грузовместимость вагона: ");
        Carriadge carriadge = new Carriadge(requestNumber());  // создаем новый вагон
        System.out.println("Новый вагон грузовместимостью (тонн): " + carriadge.getMass());

        System.out.println("Введите количество вагонов в составе: ");
        int qty = requestNumber(); // задаем количество вагонов в составе
        Carriadge[] carriadges = new Carriadge[qty];  // создаем массив вагонов
        createCarriadge(qty, carriadges);  // заполняем массив элементами

        RailwayTrain railwayTrain = new RailwayTrain(carriadges); // создаем экземпляр класса железнодорожного состава

        System.out.println("Суммарная грузоподъемность железнодорожного состава (" + qty + " вагонов): " + railwayTrain.sumMass(qty));
        System.out.println("Средняя грузовместимость на вагон " + railwayTrain.averageMass(qty));
    }

    private static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    // Метод заполнения созданного массива элементами с рандомной грузоподъемностью
    private static void createCarriadge(int quantity, Carriadge[] carriadges) {
        for (int i = 0; i < quantity; i++) {
            int mass = (int) (Math.random() * 100 + 1);  // задаем рандомное значение грузоподъемности вагона
            carriadges[i] = new Carriadge(mass);
            System.out.println("Вагон " + (i + 1) + "грузоподъемностью " + mass);
        }
    }

}
