package s06;

public class LongWordHandler extends CheckString {

    private int m;
    private int n;

    public LongWordHandler(int m, int n) {
        this.m = m;
        this.n = n;
    }

    @Override
    public String handleMessage(String message) {

        String[] words = message.split(" ");

        // Проверка на длину фразы (здесь не надо дополнительно проверять на null, т.к эта проверка входит в текущее условие)
        if (words.length < m) {
            System.out.println("Ошибка ввода: в фразе меньше " + m + " слов");
            return null;
        }

        // Проверка длины каждого из слов
        for (String word : words) {
            if (word.length() < n) {
                System.out.println("Ошибка ввода: в фразе есть слова длиной меньше " + n + " букв");
                return null;
            }
        }
        return checkNext(message);
    }
}
