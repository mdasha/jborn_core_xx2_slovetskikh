package s06;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {

        System.out.println("Создаем новый квадрат и считаем его периметр и площадь!");
        System.out.println("Введите сторону квадрата: ");
        double a = requestNumber();
        Shape square = new Square(a);
        System.out.println("Периметр квадрата равен: " + square.calculatePerimeter());
        System.out.println("Площадь квадрата равна: " + square.calculateArea());

        System.out.println("Создаем новый прямоугольник и считаем его периметр и площадь!");
        System.out.println("Введите короткую сторону прямоугольника: ");
        double b = requestNumber();
        System.out.println("Введите длинную сторону прямоугольника: ");
        double c = requestNumber();
        Shape rectangle = new Rectangle(b, c);
        System.out.println("Периметр прямоугольника равен: " + rectangle.calculatePerimeter());
        System.out.println("Площадь прямоугольника равна: " + rectangle.calculateArea());

        System.out.println("Создаем новый треугольник и считаем его периметр и площадь!");
        double[] triangleSides = setTriangleSides();

        if (checkTriangle(triangleSides)) {
            Shape triangle = new Triangle(triangleSides[0], triangleSides[1], triangleSides[2]);
            System.out.println("Периметр треугольника равен: " + triangle.calculatePerimeter());
            System.out.println("Площадь треугольника равна: " + triangle.calculateArea());
            Summarize[] figures = new Summarize[3];
            figures[0] = square;
            figures[1] = rectangle;
            figures[2] = triangle;
            System.out.println(" ");
            System.out.println("Суммарная площадь геометрических фигур (квадрата, прямоугольника и треугольника): " + summarizeFigures(figures));
        } else {
            System.out.println("Треугольника с такими сторонами не существует");
            Summarize[] figures2 = new Summarize[2];
            figures2[0] = square;
            figures2[1] = rectangle;
            System.out.println(" ");
            System.out.println("Суммарная площадь 2 геометрических фигур (квадрата, прямоугольника): " + summarizeFigures(figures2));
        }
    }

    // Создаем массив из сторон треугольника
    private static double[] setTriangleSides() {
        double[] triangle = new double[3];
        for (int i = 0; i < 3; i++) {
            System.out.println("Введите " + (i + 1) + " сторону треугольника");
            triangle[i] = requestNumber();
        }
        return triangle;
    }

    // Проверяем, могут ли составить три введенные стороны треугольник
    private static boolean checkTriangle(double[] triangle) {
        return triangle[0] < triangle[1] + triangle[2] && triangle[1] < triangle[0] + triangle[1] && triangle[2] < triangle[0] + triangle[1];
    }

    public static double summarizeFigures(Summarize[] figures) {
        double sum = 0;
        for (Summarize figure: figures) {
            sum += figure.calculateArea();
        }
        return sum;
    }

    private static double requestNumber() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }
}
