package s06;

public class LowerCaseHandler extends CheckString {

    public LowerCaseHandler(String message) {
        this.message = message;
    }

    @Override
    public String handleMessage(String message) {
        if (message == null) {
            return null;
        }
        return checkNext(message.toLowerCase().trim());
    }
}
