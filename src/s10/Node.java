package s10;

class Node<T> {
    private T value;
    Node<T> next;

    public Node(T value) {
        this.value = value;
    }

    public static boolean hasCycle (Node first) {
        Node turtle = first; // создаем узел медленного итератора, задаем ему начальное значение - первой ноды
        Node hare = first; // создаем узел быстрого итератора, задаем ему начальное значение - также первой ноды
        while (hare != null && hare.next != null) {
            turtle = turtle.next; // итерируемся по медленному итератору
            hare = hare.next.next; // итерируемся по быстрому итератору
            if (hare == turtle) {
                return true;  // Если быстрый итератор догнал медленный, то список зацикленный
            }
        }
        return false; // Если наткнулись на следующий пустой элемент, то список не зацикленный
    }

}
