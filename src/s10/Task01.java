package s10;

public class Task01 {
    public static void main(String[] args) {

        // Создаем односвязный список
        // Вводим узлы односвязного списка
        Node<Integer> node = new Node<>(78);
        Node<Integer> node1 = new Node<>(25);
        Node<Integer> node2 = new Node<>(13);
        Node<Integer> node3 = new Node<>(78);

        Node<Integer> node4 = new Node<>(35);
        Node<Integer> node5 = new Node<>(56);
        Node<Integer> node6 = new Node<>(76);
        Node<Integer> node7 = new Node<>(87);

        // Зацикливаем односвязный список
        node.next = node1;
        node1.next = node2;
        node2.next = node3;
        node3.next = node;

        // Задаем незацикленный односвязный список
        node4.next = node5;
        node5.next = node6;
        node6.next = node7;
        node7.next = null;

        // Проверяем, зациклен ли односвязный список
        cycledLinkedList(node); // Проверяем первый список
        cycledLinkedList(node4); // Проверяем второй список
    }

    private static void cycledLinkedList(Node node) {
        if (Node.hasCycle(node)) {
            System.out.println("Односвязный список зацикленный");
        } else {
            System.out.println("Односвязный список не зацикленный");
        }
    }
}