package s10;

public class Fifo<T> implements Queue<T> {

    private int capacity; // размер очереди
    private int size; // размер очереди
    private Node2<T> front;  // первый элемент в очереди
    private Node2<T> rear; // последний элемент в очереди

    public Fifo(int capacity) {
        this.capacity = capacity;
    }

    // Вывод элемента очереди
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        // Если индекс меньше, чем из середины очереди, то идем с начала очереди
        if (index <= size / 2) {
            int i = 0;
            Node2<T> current = front;
            while (i != index) {
                current = current.next;
                i++;
            }
            return current.value;

            // Если индекс больше, чем половина размера очереди, то идем с конца очереди
        } else {
           int i = size;
            Node2<T> current = rear;
            while (i != index) {
                current = current.prev;
                i--;
            }
            return current.next.value;
        }
    }

    // Размер (длина) очереди
    public int length() {
        return size;
    }

    // Добавляем элемент в конец очереди, если очередь не переполнена. Если очередь переполнена, то добавить ничего не можем
    @Override
    public boolean add(T e) {
        Node2<T> node = new Node2<>(e);

        if (size < capacity) {
            if (front == null) {
                front = node;
                rear = node;
            } else {
                rear.next = node;
                node.prev = rear;
                rear = node;
            }
            size++;
            return true;
        }
        return false;
    }

    @Override
    public T poll() {
        Node2<T> current = front;

        if (front == null) {
            return null;
        } else {
            if (current.next == null) {
                front = null;
                return current.value;
            }
            current = current.next;
            front = current;
            return current.prev.value;
        }
    }

    @Override
    public T peek() {
        Node2<T> current = front;

        if (front == null) {
            return null;
        } else {
            return current.value;
        }
    }

}

class Node2<T> {
    T value;
    Node2<T> next;
    Node2<T> prev;

    public Node2(T value) {
        this.value = value;
    }
}



