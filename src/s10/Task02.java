package s10;


public class Task02 {
    public static void main(String[] args) {
        CustomLinkedList<Integer> list = new CustomLinkedList<>();

        // Созадаем список из 10 элементов
        for (int i = 0; i < 10; i++) {
            list.put(i + 1);
        }

        // Распечатываем полученный массив
        for (int i = 0; i < list.length(); i++) {
            System.out.println(i + ": " + list.get(i));
        }

        System.out.println("Двусвязный список после вставки элементов");
        // Вставляем в список элемент со значением 78 на место индекса 8
        list.put(8, 78);
        // Вставляем в список элемент со значением 25 на место индекса 0
          list.put(0, 25);
        // Вставляем в список элемент со значением 13 на место индекса 12
         list.put(12, 13);

        // Распечатываем полученный массив
        for (int i = 0; i < list.length(); i++) {
            System.out.println(i + ": " + list.get(i));
        }

        // Создаем переменную node со значением, которое будем удалять из нашего двусвязного списка
        Node3<Integer> node = new Node3<>(78);
        Node3<Integer> node1 = new Node3<>(25);
        Node3<Integer> node2 = new Node3<>(13);
        System.out.println("Двусвязный список после удаления элементов по Node. Удаляемые элементы " + node.value + ", " + node1.value + ", " + node2.value);
        // Удаляем созданную node из нашего двусвязного списка
        list.remove(node.value);
        list.remove(node1.value);
        list.remove(node2.value);
        System.out.println("list.length (after remove): " + list.length());
        for (int i = 0; i < list.length(); i++) {
            System.out.println(i + ": " + list.get(i));
        }

        System.out.println("Двусвязный список после удаления элементов по индексу.");
        // Удаляем node под номером i из нашего двусвязного списка
        list.remove(0);
        list.remove(8);
        list.remove(2);
        System.out.println("list.length (after remove): " + list.length());
        for (int i = 0; i < list.length(); i++) {
            System.out.println(i + ": " + list.get(i));
        }
    }
}
