package s10;

public class CustomLinkedList<E> implements List<E> {

    int size;
    Node3<E> first;
    Node3<E> last;

    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return search(index).value;  // Поиск элемента по индексу
    }

    // Вставка элемента в конец списка
    @Override
    public void put(E e) {
        Node3<E> node = new Node3<>(e);
        size++;
        if (first == null) {
            first = node;
            last = node;
        } else {
            linkLast(node);
        }
    }

    @Override
    public void put(int index, E e) {
        Node3<E> node = new Node3<>(e);
        size++;
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        // Если вставляем в начало списка
        if (index == 0) {
            linkFirst(node);
        } else if (index == size - 1) {
            // Если вставляем в конец списка
            linkLast(node);
        } else {
            // Если вставляем в середину списка
            linkMiddle(node, search(index), index, size);
        }
    }


    @Override
    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node3<E> node = new Node3<>(search(index).value);
        size--;

        // Если удаляем первый элемент в списке
        if (node.value == first.value) {
            removeFirst(first);
            // Если удаляем последний элемент в списке
        } else if (node.value == last.value) {
            removeLast(last);
        } else {
            // Если удаляемый элемент - в середине списка (проходим список от второго до предпоследнего, начальный текущий элемент - второй элемент в списке)
            removeMiddle(node, search(index), index, size - 1);
        }
    }

    @Override
    public void remove(E e) {
        Node3<E> node = new Node3<>(e);
        size--;
        // Если удаляем первый элемент в списке
        if (node.value == first.value) {
            removeFirst(first);
            // Если удаляем последний элемент в списке
        } else if (node.value == last.value) {
            removeLast(last);
        } else {
            // Если удаляемый элемент - в середине списка (проходим список от второго до предпоследнего, начальный текущий элемент - второй элемент в списке)
            removeMiddle(node, first.next, 1, size - 1);
        }
    }


    public void removeFirst(Node3<E> current) {
        first = current.next;
        current.prev = null;
    }

    public void removeLast(Node3<E> current) {
        last = current.prev;
        current.next = null;
    }

    // Удаление элемента в середине списка
    public void removeMiddle(Node3<E> node, Node3<E> current, int start, int end) {
        for (int i = start; i < end; i++) {
            if (current != null) {
                if (current.value == node.value) {
                    current.next.prev = current.prev;
                    current.prev.next = current.next;
                    current = current.next;
                } else {
                    current = current.next;
                }
            }
        }
    }

    // Присоединяем элемент в начало списка
    public void linkFirst(Node3<E> node) {
        first.prev = node;
        node.next = first;
        first = node;
    }

    // Присоединяем элемент в конец списка
    public void linkLast(Node3<E> node) {
        last.next = node;
        node.prev = last;
        last = node;
    }

    public void linkMiddle(Node3<E> node, Node3<E> current, int start, int end) {
        for (int i = start; i < end; i++) {
            if (current != null) {
                if (i == start) {
                    node.next = current.next;
                    current.next.prev = node;
                    current.next = node;
                    node.prev = current;
                } else {
                    i++;
                    current = current.next;
                }
            }
        }
    }

    // Поиск Node по индексу
    public Node3<E> search(int index) {
        if (index < size / 2) {
            int i = 0;
            Node3<E> current = first;
            while (i != index) {
                current = current.next;
                i++;
            }
            return current;
        } else {
            int i = size;
            Node3<E> current = last;
            while (i != index + 1) {
                current = current.prev;
                i--;
            }
            return current;
        }
    }

    @Override
    public int length() {
        return size;
    }

}

class Node3<T> {
    T value;
    Node3<T> next;
    Node3<T> prev;

    public Node3(T value) {
        this.value = value;
    }

}