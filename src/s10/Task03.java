package s10;

public class Task03 {
    public static void main(String[] args) {

        // Создаем новую очередь
        int capacity = 5;
        Fifo<Integer> fifo = new Fifo<>(capacity);

        // poll для пустой очереди
        System.out.println("Poll для пустой очереди: " + fifo.poll());
        System.out.println();

        // peek для пустой очереди
        System.out.println("Peek для пустой очереди: " + fifo.peek());
        System.out.println();

        System.out.println("Емкость очереди равна " + capacity);
        System.out.println("Добавляем " + (capacity + 1) + " элементов в очередь");
        System.out.println();
        // Добавляем элементы в конец очереди. Специально пытаемся добавить на 1 элемент больше указанной емкости очереди
        for (int i = 0; i < capacity + 1; i++) {
            if (fifo.add(i + 2)) {
                System.out.println("Элемент добавлен в конец очереди");
            } else {
                System.out.println("Превышена емкость очереди. Элемент не добавлен");
            }
        }

        System.out.println();
        System.out.println("Распечатываем добавленные элементы из очереди");

        // Выводим все элементы из очереди. Видим, что последний элемент в очередь не добавлен
        for (int i = 0; i < fifo.length(); i++) {
            System.out.println(fifo.get(i));
        }

        // poll для заполненной очереди
        System.out.println();

        for (int i = 0; i < fifo.length() - 2; i++) {
            System.out.println("Poll для заполненной очереди " + (i + 1) + ": " + fifo.poll());
        }

        for (int i = fifo.length() - 2; i < fifo.length(); i++) {
            System.out.println("Peek для заполненной очереди " + (i + 1) + ": " + fifo.peek());
        }

        for (int i = fifo.length() - 2; i < fifo.length() + 2; i++) {
            System.out.println("Poll для заполненной очереди " + (i + 3) + ": " + fifo.poll());
        }

        System.out.println("Peek для заполненной очереди " + (fifo.length() + 5) + ": " + fifo.peek());
        System.out.println();
    }
}
