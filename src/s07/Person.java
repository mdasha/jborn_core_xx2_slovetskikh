package s07;

import java.time.LocalDate;

public class Person {
    private String surname;
    private String name;
    private String secondName;
    private LocalDate dateOfBirthLocalDate;
    private int age;

    public Person() {

    }

    @Override
    public String toString() {
        return
                "Распечатываем персону: " + "\n" + surname + " " + name + " " + secondName + "\n" +
                        "Дата рождения: " + dateOfBirthLocalDate + "\n" +
                        "Возраст: " + age + "\n";
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getAge() {
        return age;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirthLocalDate;
    }

    public String setName(String name) {
        return this.name = name;
    }

    public String setSurname(String surname) {
        return this.surname = surname;
    }

    public String setSecondname(String secondname) {
        return this.secondName = secondname;
    }

    public int setAge(int age) {
        return this.age = age;
    }

    public LocalDate setDateOfBirth(LocalDate dateOfBirthLocalDate) {
        return this.dateOfBirthLocalDate = dateOfBirthLocalDate;
    }

}
