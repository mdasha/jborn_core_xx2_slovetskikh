package s07;

public class DriverValidator2 implements Validator<Driver> {

    private int validAge;
    private String validLicenceCategory1;
    private String validLicenceCategory2;
    private String validLicenceCategory3;

    public DriverValidator2(int validAge, String validLicenceCategory1, String validLicenceCategory2, String validLicenceCategory3) {
        this.validAge = validAge;
        this.validLicenceCategory1 = validLicenceCategory1;
        this.validLicenceCategory2 = validLicenceCategory2;
        this.validLicenceCategory3 = validLicenceCategory3;
    }

    @Override
    public boolean isValid(Driver driver) {

        if (driver.getAge() > validAge && driver.getActualLicense() && (driver.getLicenceCategory().equals(validLicenceCategory1) || driver.getLicenceCategory().equals(validLicenceCategory2) || driver.getLicenceCategory().equals(validLicenceCategory3))) {
            System.out.println("\n" + "Водителю больше 18 лет, права действительны, категория прав B, C или D." + "\n" + "Вы можете водить легковой автомобиль" + "\n");
            return true;
        }
        System.out.println("\n" + "У вас недействительны права, вы не подходите по возрасту или по категории прав (ниже B)." + "\n" + "Вы не можете водить легковой автомобиль." + "\n");
        return false;
    }
}

