package s07;

public class DriverIntoPersonConverter implements Converter<Driver, Person> {


    @Override
    public Person convert(Driver driver) {

        Person person = new Person();

        person.setName(driver.getName());
        person.setSurname(driver.getSurname());
        person.setSecondname(driver.getSecondName());
        person.setAge(driver.getAge());
        person.setDateOfBirth(driver.getDateOfBirth());

        return person;
    }
}
