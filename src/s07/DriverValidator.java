package s07;

public class DriverValidator implements Validator<Driver> {

    private int validAge;
    private String validLicenceCategory;
    private String validAnnualPhysicalExam;


    public DriverValidator(int validAge, String validLicenceCategory, String validAnnualPhysicalExam) {
        this.validAge = validAge;
        this.validLicenceCategory = validLicenceCategory;
        this.validAnnualPhysicalExam = validAnnualPhysicalExam;
    }

    @Override
    public boolean isValid(Driver driver) {

        if (driver.getAge() > validAge && driver.getActualLicense() && driver.getLicenceCategory().equals(validLicenceCategory) && driver.getAnnualPhysicalExam().equals(validAnnualPhysicalExam)) {
            System.out.println("\n" + "Водителю больше 21 года, права действительны, вы прошли ежегодный медицинский осмотр, категория прав D." + "\n" + "Вы можете водить автобус");
            return true;
        }
        System.out.println("\n" + "У вас недействительны права, вы не подходите по возрасту, по категории прав (не D) или не прошли ежегодный медицинский осмотр." + "\n" + "Вы не можете водить автобус");
        return false;
    }
}
