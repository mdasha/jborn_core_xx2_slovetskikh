package s07;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Driver {

    private String surname;
    private String name;
    private String secondName;
    private LocalDate dateOfBirthLocalDate;
    private String drivingLicenseSeries;
    private String drivingLicenseNumber;
    private LocalDate licenseNumberDateLocalDate;
    private int age;
    private String annualPhysicalExam;
    private boolean actualLicence;
    private String licenceCategory;

    public Driver(String surname, String name, String secondName, String dateOfBirth, String drivingLicenseSeries, String drivingLicenseNumber, String licenseNumberDate, String annualPhysicalExam, String licenceCategory) {
        this.surname = surname;
        this.name = name;
        this.secondName = secondName;
        this.dateOfBirthLocalDate = LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("dd-LL-yyyy"));
        this.drivingLicenseSeries = drivingLicenseSeries;
        this.drivingLicenseNumber = drivingLicenseNumber;
        this.licenseNumberDateLocalDate = LocalDate.parse(licenseNumberDate, DateTimeFormatter.ofPattern("dd-LL-yyyy"));
        this.age = (LocalDate.now()).getYear() - (dateOfBirthLocalDate.getYear());  // вычисляем возраст водителя
        this.annualPhysicalExam = annualPhysicalExam; // ежегодная диспансеризация (да или нет)
        this.actualLicence = (LocalDate.now().isAfter(licenseNumberDateLocalDate));
        this.licenceCategory = licenceCategory;
    }

    @Override
    public String toString() {
        return
                "Проверяем водителя: " + surname + " " + name + " " + secondName + "\n" +
                        "Дата рождения: " + dateOfBirthLocalDate + "\n" +
                        "Возраст водителя: " + age + "\n" +
                        "Номер и серия прав: " + drivingLicenseSeries + " " + drivingLicenseNumber + "\n" +
                        "Права выданы: " + licenseNumberDateLocalDate + "\n" +
                        "Ежегодный медицинский осмотр пройден: " + annualPhysicalExam + "\n" +
                        "Категория прав: " + licenceCategory;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getAge() {
        return age;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirthLocalDate;
    }

    public String getAnnualPhysicalExam() {
        return annualPhysicalExam;
    }

    public boolean getActualLicense() {
        return actualLicence;
    }

    public String getLicenceCategory() {
        return licenceCategory;
    }

}