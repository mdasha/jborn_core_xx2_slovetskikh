package s07;

public class Task03 {
    public static void main(String[] args) {


        // Сравниваем Long и Double
        System.out.println("Первое сравнение (Long и Double)!");
        Double number1 = 5.0;
        System.out.println("Первое число: " + number1);

        Long number2 = 5L;
        System.out.println("Второе число: " + number2);

        NumberComparison<Double, Long> numberComparison = new NumberComparison(number1, number2);

        // сравниваем два числа и выводим комментарии к сравнению и результат сравнения
        compareComments(numberComparison.compareNumbers(number1, number2));
        System.out.println(numberComparison.compareNumbers(number1, number2));
        System.out.println(" ");

        // Сравниваем Long и Long
        System.out.println("Второе сравнение (Long и Long)!");
        Long number5 = Long.MAX_VALUE;
        System.out.println("Первое число: " + number5);

        Long number6 = Long.MAX_VALUE - 1;
        System.out.println("Второе число: " + number6);

        NumberComparison<Long, Long> numberComparison3 = new NumberComparison(number5, number6);

        // сравниваем два числа и выводим комментарии к сравнению и результат сравнения
        compareComments(numberComparison3.compareNumbers(number5, number6));
        System.out.println(numberComparison3.compareNumbers(number5, number6));
        System.out.println(" ");

        // Сравниваем Double и Float
        System.out.println("Второе сравнение (Double и Float)!");
        Double number3 = 5.11;
        System.out.println("Первое число: " + number3);

        Float number4 = 5.112f;
        System.out.println("Второе число: " + number4);


        NumberComparison<Double, Float> numberComparison2 = new NumberComparison(number1, number2);

        // сравниваем два числа и выводим комментарии к сравнению и результат сравнения
        compareComments(numberComparison2.compareNumbers(number3, number4));
        System.out.println(numberComparison2.compareNumbers(number3, number4));

    }

    // Вывод комментариев к сравниваемым числам
    public static void compareComments(int number) {

        switch (number) {
            case (0):
                System.out.print("Сравниваемые числа равны друг другу: ");
                break;
            case (1):
                System.out.print("Первое число больше второго: ");
                break;
            case (-1):
                System.out.print("Второе число больше первого: ");
                break;
        }
    }
}
