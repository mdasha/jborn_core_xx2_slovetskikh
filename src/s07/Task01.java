package s07;

public class Task01 {

    public static void main(String[] args) {

        Driver Ivanov = new Driver("Иванов", "Иван", "Иванович", "31-08-1999", "23 KC", "018834", "10-10-2018", "да", "C");
        DriverValidation(Ivanov); // выводим характеристики водителя Иванова и проверяем два условия (возоможность водить автобус и легковой автомобиль)
        DriverIntoPersonConverter(Ivanov);  // конвертируем водителя Иванова в перосну Иванова

        Driver Petrov = new Driver("Петров", "Петр", "Петрович", "31-08-1995", "24 MC", "015678", "10-10-2018", "да", "D");
        DriverValidation(Petrov); // выводим характеристики водителя Петрова и проверяем два условия (возоможность водить автобус и легковой автомобиль)
        DriverIntoPersonConverter(Petrov); // конвертируем водителя Петрова в персону Петрова
    }


    // Валидация водителей и распечатка данных по водителям
    private static void DriverValidation(Driver driver) {
        System.out.println(driver.toString()); // Выодим характеристики водителя

        DriverValidator DriverValidation = new DriverValidator(21, "D", "да"); // проверяем водителя автобуса
        DriverValidation.isValid(driver);

        DriverValidator2 DriverValidation2 = new DriverValidator2(18, "B", "C", "D"); // проверяем водителя легкового автомобиля
        DriverValidation2.isValid(driver);
    }

    // Конвертер водителя в персону и распечатка данных по персоне
    private static void DriverIntoPersonConverter(Driver driver) {
        DriverIntoPersonConverter DriverIntoPersonConverter = new DriverIntoPersonConverter();
        System.out.println(DriverIntoPersonConverter.convert(driver).toString());
    }



}
