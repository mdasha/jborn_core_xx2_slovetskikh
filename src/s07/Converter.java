package s07;

public interface Converter<FROM, INTO> {

    INTO convert(FROM From);

}
