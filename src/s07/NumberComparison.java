package s07;

import java.lang.Number;
import java.math.BigDecimal;

public class NumberComparison<N1 extends Number, N2 extends Number> {

    private N1 number1;
    private N2 number2;


    public NumberComparison(N1 number1, N2 number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public int compareNumbers(N1 number1, N2 number2) {
        // Если хотя бы одно из пришедших чисел дробное, то приводим их к Double и сравниваем между собой
        if ((number1 instanceof Double || number1 instanceof Float) || (number2 instanceof Double || number2 instanceof Float)) {
            BigDecimal changedNumber1 = new BigDecimal(number1.toString());
            BigDecimal changedNumber2 = new BigDecimal(number2.toString());
            return changedNumber1.compareTo(changedNumber2);

         // В остальных случаях приводим к Long
        } else {
            Long changedNumber1 = number1.longValue();
            Long changedNumber2 = number2.longValue();
            return changedNumber1.compareTo(changedNumber2);
        }
    }
}