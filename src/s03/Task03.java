package s03;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        String randomString = requestString();

        for (int i = 0; i < randomString.length(); i++) {
            for (int k = 1; k < i + 2; k++) {
                System.out.print(randomString.charAt(i));
            }
            System.out.println("");
        }

    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите слово:");
        return scanner.nextLine();
    }
}
