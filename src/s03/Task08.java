package s03;

import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        System.out.println("Введите какую-нибуь фразу: ");
        String randomString = requestString();
        String newString = "";
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < randomString.length(); i++) {
            builder.append(randomString.charAt(randomString.length() - i - 1));
        }

        newString = builder.toString();
        System.out.println(newString);
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
