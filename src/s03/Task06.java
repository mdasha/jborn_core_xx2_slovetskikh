package s03;

import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {

        System.out.println("Введите строку: ");
        String randomString = requestString();
        System.out.println("Введите один из символов введенной строки: ");
        Character randomChar = requestString().charAt(0);
        int qty = 0 ; // переменная для подсчета количества вхождений символа в строку
        String newRandomString = "";

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < randomString.length(); i++) {
            Character searchedChar = randomString.charAt(i);

            // Проверяем равна ли текущая буква введенному символу. Если да, то увеличиваем счетчик на 1 и переводим текущую букву в верхний регистр
            if (searchedChar.equals(randomChar)) {
                qty += 1;  // считаем количество введенных символов во введенной строке
                searchedChar = searchedChar.toUpperCase(searchedChar); // переводим заданный символ в верхний регистр

            }

            builder.append(searchedChar);
        }
        newRandomString = builder.toString();
        System.out.println("Количество вхождений символа " + randomChar + " в строку " + randomString + " равно: " + qty);
        System.out.println("Новая строка " + newRandomString);

    }


    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }


}
