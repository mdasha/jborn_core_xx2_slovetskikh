package s03;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        String randomString = requestString();
        int stringLength = randomString.length();
        int i = 0;

        for (i = 0; i < stringLength/2; i++) {
            if (randomString.charAt(i) != randomString.charAt(stringLength - i - 1)) {
                System.out.println("Это не палиндром");
                i = 1;
                break;
            }
        }
            if (i != 1) {
                System.out.println("Это палиндром");
            }

    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите произвольную строку:");
        return scanner.nextLine();
    }
}
