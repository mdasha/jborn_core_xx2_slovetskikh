package s03;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int register = requestNumber();
        String sum = "";
        int start = 0;
        int end = 9;
        int random;

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < register; i++) {
            random = start + (int) (Math.random() * end);
            builder.append(random);
        }
        sum = builder.toString();
        System.out.println("ПИН код:" + sum);
    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите регистр:");
        return scanner.nextInt();
    }
}
