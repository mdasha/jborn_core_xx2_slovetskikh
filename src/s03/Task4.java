package s03;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        System.out.println("Введите ширину и длину прямоугольника");

        int width = requestNumber();
        int len = requestNumber();

        for (int i = 0; i < len; i++) {
            for (int k = 0; k < width; k++) {

                if (i == 0 || i == len - 1 || k == 0 || k == width - 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }
    }


    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        return scanner.nextInt();
    }

}
