package s03;

import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        System.out.println("Введите строку, содержащую символы нижнего и верхнего регистра: ");
        String randomString = requestString();

        for (int i = 0; i < randomString.length(); i++) {
            Character nextChar = randomString.charAt(i);  // выделяем текущий проверяемый символ
            boolean lowerCase = Character.isLowerCase(nextChar); // вводим переменную для проверки регистра

            if (lowerCase) {
                System.out.print(nextChar.toString().toUpperCase()); // если текущий символ в нижнем регистре, то выводим его в верхнем регистре
            } else {
                System.out.print(nextChar.toString().toLowerCase()); // если наоборот, то выводим его в нижнем регистре
            }
        }
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
