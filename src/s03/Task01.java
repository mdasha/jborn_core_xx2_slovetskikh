package s03;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {

        System.out.println("Введите название государства: ");
        String state = requestString();

        System.out.println("Введите столицу этого государства");
        String capital = requestString();

        System.out.println("Столица государства " + state + " - город " + capital);
    }
    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
