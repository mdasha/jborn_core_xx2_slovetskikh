package s03;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        System.out.println("Введите предложение");
        String sentence = requestString();

            if (sentence.contains("жи") || sentence.contains("ши")) {
                System.out.println("Правило написания жи/ши выполнено верно ");
            } else if (sentence.contains("жы") || sentence.contains("шы")) {
                System.out.println("Жи/ши пиши с буквой и. Исправль введенную фразу");
            } else {
                System.out.println("В фразе нет сочетания жи или ши");
            }
    }

    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
