package s03;

import java.util.Scanner;

public class Task09 {
    public static void main(String[] args) {
        int arrayLength = requestNumber();
        String array[];
        array = new String[arrayLength];
        StringBuilder builder = new StringBuilder();
        String result =":";

        for (int i = 0; i < arrayLength; i++) {
            System.out.println("Введите элемент массива " + (i + 1));
            array[i] = requestString();
            builder.append(array[i]);
            if (i < arrayLength - 1) {
                String comma = ", ";
                builder.append(comma);
            }
        }

        result = builder.toString();
        System.out.println(result);

    }

    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива:");
        return scanner.nextInt();
    }
    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

}
