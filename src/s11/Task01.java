package s11;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {

        // Задаем с клавиатуры количество элементов массива
        int qty = requestNumber();
        int []  quickSortArray = new int[qty];

       // Заполняем массив случайными числами до 100
       Random rnd = new Random();
       for (int i = 0; i < qty; i++) {
           quickSortArray[i] = Math.abs(rnd.nextInt(100));
       }
        System.out.println("Исходный массив: \n" + Arrays.toString(quickSortArray));

        int left = 0;  // левая граница
        int right = qty - 1; // правая граница

        quickSort(quickSortArray, left, right);

        System.out.println("Отсортированный массив: \n" + Arrays.toString(quickSortArray));
    }

    // Задаем с клавиатуры количество элементов массива для сортировки
    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество элементов массива:");
        return scanner.nextInt();
    }

    private static void quickSort(int [] quickSortArray, int left, int right) {

        // Если длина массива нулевая, то завершаем работу метода
        if (quickSortArray.length == 0) {
            return;
        }

        // Если уже нечего делить, то завершаем выполнение
        if (left >= right) {
            return;
        }

        // Выбираем опорный элемент
        int opora = quickSortArray[right];


        int i = left;
        int j = right;
        while (i <= j) {
            while (quickSortArray[j] > opora) {
                j--;
            }

            while (quickSortArray[i] < opora) {
                i++;
            }

            // Меняем элементы местами
            if (i <= j) {
                int tmp = quickSortArray[i];
                quickSortArray[i] = quickSortArray[j];
                quickSortArray[j] = tmp;
                i++;
                j--;
            }
        }

        // Рекурсивно вызываем наш метод для сортировки левого и правого подмассивов
        if (i < right) {
           quickSort(quickSortArray, i, right);
        }
        if (j > left) {
            quickSort(quickSortArray, left, j);
        }
    }
}
