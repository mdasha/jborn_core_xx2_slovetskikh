package s04;

import java.util.Locale;
import java.util.Scanner;

public class Circle {
    private double radius;
    private double x;
    private double y;

    // Вычисляем площадь круга
    public double squarecalc(Circle circle) {
       return Math.PI * Math.pow(circle.radius, 2);
    }

    // Перемещение центра окружности
    public double[] replaceCircle(double x1, double y1) {
        return new double[]{x + x1, y + y1};
    }

    // Вычисляем расстояние между двумя окружностями
    public double distance(Circle circle) {
        return Math.sqrt(Math.pow((circle.y - this.y),2) + Math.pow((circle.x - this.x),2));
    }

    // Проверка на пересечение окружностей
    public Boolean cross(Circle circle) {
        boolean cross;
        double sumRadius = circle.radius + this.radius; // сумма радиусов окружностей
        double modulDiff = Math.abs(circle.radius - this.radius); // модуль разности радиусов окружностей
        // Проверка условий для определения пересекаются ли окружности хотя бы в одной точке
        if (distance(circle) < sumRadius && distance(circle) > modulDiff) {
           cross = true;
        } else {
            cross = false;
        }
        return cross;
    }

    // Вводим исходные данные с клавиатуры
    public Circle() {
        System.out.println("Введите радиус окружности:");
        radius = requestNumber();
        System.out.println("Введите координату x центра окружности");
        x = requestNumber();
        System.out.println("Введите координату y центра окружности");
        y = requestNumber();
    }
    static double requestNumber() {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);
        return scanner.nextDouble();
    }
    
}
