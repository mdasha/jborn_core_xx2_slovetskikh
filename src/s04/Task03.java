package s04;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {

        // вводим с клавиатуры номер числа Фибоначчи
        int fibonachi = requestNumber();

        // Выводим значение n-го числа Фибоначчи
        System.out.println(fibo(fibonachi));
    }

    static int fibo(int n) {
       if (n == 1) {
           return 1;
       }
       if (n == 2) {
           return 1;
       }
       return fibo(n - 1) + fibo(n - 2);
    }



    static int requestNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите целоое число n(для расчета n-го числа Фиббоначчи:");
        return scanner.nextInt();
    }
}
