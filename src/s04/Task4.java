package s04;

public class Task4 {
    public static void main(String[] args) {
        System.out.println("Введите исходные данные!");
        System.out.println("Введите данные для первой окружности: ");
        Circle circle1 = new Circle();
        System.out.println("Введите данные для второй окружности: ");
        Circle circle2 = new Circle();
        System.out.println("Результаты вычислений: ");
        System.out.println("1. Площадь круга, ограниченного первой окружностью: " + circle1.squarecalc(circle1));  // выводим площадь круга
        System.out.println("2. Смещаем первую окружность на 10.5 по x и на 20.7  по y");
        System.out.println("Новые координаты центра окружности: x = " + circle1.replaceCircle(10.5, 20.7)[0] + "; y = " + circle1.replaceCircle(10.5, 20.7)[1]);
        System.out.println("3. Проверка на пересечение окружностей: ");
        if (circle1.cross(circle2)) {
            System.out.println("Окружности пересекаются");
        } else {
            System.out.println("Окружности не пересекаются");
        }
        System.out.println("4. Расстояние между окружностями: " + circle1.distance(circle2));
    }
}
