package s04;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        // вводим строку с клавиатуры
        String newString = requestString();
        // вызываем рекурсивный метод
        System.out.println(inverse(newString));
    }

    // Метод ввода строки с клавиатуры
    static String requestString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку");
        return scanner.nextLine();
    }


    // Рекурсивный метод вывода строки наоборот

    public static String inverse(String newString) {
        // база рекурсии (выход из рекурсии, когда переберем все символы, кроме первого).
        if ((newString == null) || (newString.length() <= 1)) {
            return newString;
        }
        return inverse(newString.substring(1)) + newString.charAt(0);
    }


}
