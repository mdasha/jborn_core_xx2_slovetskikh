package s04;

public class Task1 {
    public static void main(String[] args) {
        Hero hero = new Hero();

        Hero rox = new Hero(20);
        Hero nyx = new Hero(40);
        System.out.println("Статус игрока nyx до битвы");
        nyx.printStatus();
        rox.hit(nyx);
        System.out.println("Статус игрока nyx после битвы");
        nyx.printStatus();
        System.out.println("Характеристики игроков после битвы:");
        System.out.println("nyx health: " + nyx.getHealth());
        System.out.println("nyx power: " + nyx.getPower());
        System.out.println("nyx speed: " + nyx.getSpeed());
        System.out.println("++++++++++++++++++++++++++++++++++++");
        System.out.println("rox health: " + rox.getHealth());
        System.out.println("rox power: " + rox.getPower());
        System.out.println("rox speed: " + rox.getSpeed());
    }
}
