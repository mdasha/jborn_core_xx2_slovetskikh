package s14;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.HashMap;

public class Task01 {
    public static void main(String[] args) throws IOException, InterruptedException {

        String str = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        Integer [] lettersCount = new Integer[str.length()];
        LetterRunnable [] letters = new LetterRunnable[str.length()];
        Thread [] threads = new Thread[str.length()];
        InputStream [] fis = new InputStream[str.length()];
        System.out.println("Неотсортированный массив вхождений букв в текст");

        for (int i = 0; i < str.length(); i++) {
            fis[i] = new FileInputStream("src/s14/text.txt");
            letters[i] = new LetterRunnable(i, fis[i], Charset.forName("UTF-8"), str);
            threads[i] = new Thread(letters[i]);
            threads[i].start();
        }

        for (Thread thread : threads) {
            thread.join();
        }

        HashMap<Character, Integer> map = new HashMap<>();
        // Получаем массив результатов каждого из потоков
       for (int i = 0; i < str.length(); i++) {
           lettersCount[i] = letters[i].getResult();
           map.put(str.charAt(i), lettersCount[i]);
       }

        System.out.println("==========================================================================");
        System.out.println("Отсортированный массив вхождений букв в текст");
        map.entrySet().stream()
                .sorted(Map.Entry.<Character, Integer>comparingByValue().reversed())
                .forEach(System.out::println);
    }
}

class LetterRunnable implements Runnable {

    private final int i;
    private final String str;
    private int result;
    private final InputStream inputStream;
    private final Charset charset;

    public LetterRunnable(int i, InputStream inputStream, Charset charset, String str) {
        this.i = i;
        this.inputStream = inputStream;
        this.charset = charset;
        this.str = str;
    }

    @Override
    public void run() {
        StringBuilder stringBuilder = new StringBuilder();
        try (
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, charset))
        ) {
            String line;
            int count = 0;
            while ((line = reader.readLine()) != null) {
                for (int j = 0; j < line.length(); j++) {
                    if (line.substring(j, j+1).equals(str.substring(i, i + 1))) {
                        count++;
                    }
                }
                stringBuilder.append(line.toLowerCase()).append("\n");
            }
            reader.close();
            result = count;
      System.out.println(str.charAt(i) + ":" + count);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public int getResult() {
        return result;
    }
}
